<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraineesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('position',50);
            $table->string('firstname',50);
            $table->string('middlename',50);
            $table->string('lastname',50);
            $table->string('passport_no',50);
            $table->string('seamans_no',50);
            $table->string('mobile',50);
            $table->date('dob',50);
            $table->string('email',50)->unique();
            $table->string('emergency_person',50);
            $table->string('emergency_contact',50);
            $table->boolean('is_deleted');
            $table->integer('created_by');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trainees');
    }
}
