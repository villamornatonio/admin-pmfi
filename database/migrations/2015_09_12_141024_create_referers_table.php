<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referrers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('firstname',60);
            $table->string('lastname',60);
            $table->string('contact_no',60);
            $table->string('company',60);
            $table->string('email',60)->unique();
            $table->boolean('is_deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('referrers');
    }
}
