<?php

use App\Models\Referrer;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ReferrersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Faker::create();
        foreach (range(1, 30) as $index) {
            Referrer::create([
                'firstname' => $fake->firstName,
                'lastname' => $fake->lastName,
                'contact_no' => $fake->phoneNumber,
                'company' => $fake->company,
                'email' => $fake->companyEmail,
                'is_deleted' => 0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ]);
        }

    }
}
