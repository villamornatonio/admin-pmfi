<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 8/9/15
 * Time: 5:16 PM
 */


use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        $Users = [
            ['firstname' => 'Villamor',
                'lastname' => 'Natonio Jr',
                'email' => 'admin@pmfi.ph',
                'password' => Hash::make('password'),
                'role_id' => '1',
                'is_activated' => 1,
                'is_deleted' => 0,
                'avatar' => 'profile.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime],

            ['firstname' => 'Villamor',
                'lastname' => 'Natonio Jr',
                'email' => 'finance@pmfi.ph',
                'password' => Hash::make('password'),
                'role_id' => '2',
                'is_activated' => 1,
                'is_deleted' => 0,
                'avatar' => 'profile.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime],
            ['firstname' => 'Villamor',
                'lastname' => 'Natonio Jr',
                'email' => 'accounting@pmfi.com',
                'password' => Hash::make('password'),
                'role_id' => '3',
                'is_activated' => 1,
                'is_deleted' => 0,
                'avatar' => 'profile.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime],
            ['firstname' => 'Villamor',
                'lastname' => 'Natonio Jr',
                'email' => 'cashier@pmfi.com',
                'password' => Hash::make('password'),
                'role_id' => '3',
                'is_activated' => 1,
                'is_deleted' => 0,
                'avatar' => 'profile.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime],

            ['firstname' => 'Villamor',
                'lastname' => 'Natonio Jr',
                'email' => 'registrar@pmfi.com',
                'password' => Hash::make('password'),
                'role_id' => '4',
                'is_activated' => 1,
                'is_deleted' => 0,
                'avatar' => 'profile.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime],
            ['firstname' => 'Villamor',
                'lastname' => 'Natonio Jr',
                'email' => 'user@pmfi.com',
                'password' => Hash::make('password'),
                'role_id' => '4',
                'is_activated' => 1,
                'is_deleted' => 0,
                'avatar' => 'profile.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime],
        ];

        DB::table('users')->insert($Users);

        $fake = Faker::create();

        $roles_ids = Role::all()->lists('id')->toArray();

        foreach(range(1, 30) as $index){
            User::create([
                'firstname' => $fake->firstName,
                'lastname' => $fake->lastName,
                'email' => $fake->email,
                'password' => Hash::make('password'),
                'role_id' => $fake->randomElement($roles_ids),
                'is_activated' => 1,
                'is_deleted' => 0,
                'avatar' => $fake->imageUrl(),
                'contact_no' => $fake->phoneNumber,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ]);
        }
    }

}