<?php

use App\Models\Course;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Faker::create();

        $user_ids = User::all()->lists('id')->toArray();

        foreach(range(1, 200) as $index){
            Course::create([
                'name' => $fake->word(),
                'code' => $fake->word(),
                'price' => $fake->randomNumber(),
                'duration' => $fake->randomNumber(),
                'created_by' => $fake->randomElement($user_ids),
                'is_deleted' => 0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ]);
        }
    }
}
