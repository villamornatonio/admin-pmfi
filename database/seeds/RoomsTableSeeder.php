<?php

use App\Models\Room;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Faker::create();

        $user_ids = User::all()->lists('id')->toArray();
        foreach(range(1, 30) as $index){
            Room::create([
                'name' => $fake->numberBetween(199,300),
                'created_by' => $fake->randomElement($user_ids),
                'is_deleted' => 0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ]);
        }
    }
}
