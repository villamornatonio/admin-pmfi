<?php

use App\Models\Course;
use App\Models\Enrollment;
use App\Models\Referrer;
use App\Models\Room;
use App\Models\Trainee;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class EnrollmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Faker::create();

        $user_ids = User::all()->lists('id')->toArray();
        $trainee_ids = Trainee::all()->lists('id')->toArray();
        $room_ids = Room::all()->lists('id')->toArray();
        $courses_ids = Course::all()->lists('id')->toArray();
        $referrer_ids = Referrer::all()->lists('id')->toArray();




        foreach(range(1, 100) as $index){
            Enrollment::create([
                'trainee_id' => $fake->randomElement($trainee_ids),
                'room_id' => $fake->randomElement($room_ids),
                'course_id' => $fake->randomElement($courses_ids),
                'referred_by' => $fake->randomElement($referrer_ids),
                'is_company_charged' => $fake->boolean(),
                'created_by' => $fake->randomElement($user_ids),
                'start_date' => $fake->date('Y-m-d'),
                'end_date' => $fake->date('Y-m-d'),
                'start_time' => $fake->time(),
                'end_time' => $fake->time(),
                'company' => $fake->company(),
                'is_deleted' => 0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ]);
        }
    }
}

