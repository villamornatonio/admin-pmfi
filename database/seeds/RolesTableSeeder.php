<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 8/9/15
 * Time: 5:16 PM
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class RolesTableSeeder extends Seeder
{

    public function run()
    {

        $Roles = [
            ['name' => 'Admin', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'Finance', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'Accounting', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'Cashier', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'Registrar', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'User', 'created_at' => new DateTime, 'updated_at' => new DateTime],
        ];


        DB::table('roles')->insert($Roles);

    }

}