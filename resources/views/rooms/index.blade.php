@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('rooms') }}}">Rooms</a></li>
            <li class="active">Display Room List</li>
        </ol>


        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Rooms List</h4>

                <p>Manage Rooms</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>ROOM ID</th>
                            <th>ROOM NAME</th>
                            <th>AUTHOR</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            'use strict';

            $('#data-display').DataTable({
                ajax: 'api/v1/rooms',
                "columnDefs": [ {
                    "targets": 3,
                    render: function (data, type, row) {
                        console.log(row);
                        var actions = "<ul class='table-options'><li><a href='/rooms/" + row['id'] + "/edit" + "'><i class='fa fa-pencil'></i></a></li><li><a class='delete' href='#'><i class='fa fa-trash'></i></a></li></ul>"
                        return actions;
                    }
                } ],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'author', name: 'author'},
                ]

            });

        });
    </script>
@endsection