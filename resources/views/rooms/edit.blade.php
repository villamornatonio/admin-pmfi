@extends('layouts.master')
@section('content')

    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('rooms') }}}">Rooms</a></li>
            <li class="active">Edit Room</li>
        </ol>

        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading nopaddingbottom">
                        <h4 class="panel-title">Edit Room</h4>

                        <p>Please provide the necessary information on the fields</p>
                    </div>
                    <div class="panel-body">
                        <hr>
                        {!! Form::open([
                        'url' => 'rooms',
                        'method' => 'PUT',
                        'class' => 'form-horizontal'

                        ]) !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Room Name <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('name',null,['class' => 'form-control' ,'placeholder' => 'example: 209',
                                'required' => true]) !!}
                            </div>
                        </div>


                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-success btn-quirk btn-wide mr5">Update</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel -->

            </div>
            <!-- col-md-6 -->

        </div>
        <!--row -->

    </div><!-- contentpanel -->
@endsection