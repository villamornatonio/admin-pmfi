@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('referrers') }}}">Referrers</a></li>
            <li class="active">Display Referrers List</li>
        </ol>


        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Referrers List</h4>

                <p>Manage Referrer</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>REFERRER ID</th>
                            <th>NAME</th>
                            <th>COMPANY</th>
                            <th>MOBILE</th>
                            <th>EMAIL</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            'use strict';

            $('#data-display').DataTable({
                ajax: 'api/v1/referrers',
                "columnDefs": [ {
                    "targets": 5,
                    render: function (data, type, row) {
                        console.log(row);
                        var actions = "<ul class='table-options'><li><a href='/referrers/" + row['id'] + "/edit" + "'><i class='fa fa-pencil'></i></a></li><li><a class='delete' href='#'><i class='fa fa-trash'></i></a></li></ul>"
                        return actions;
                    }
                } ],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'company', name: 'company'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'email', name: 'email'}
                ]

            });

        });
    </script>
@endsection