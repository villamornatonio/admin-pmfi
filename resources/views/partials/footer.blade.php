<script src="/lib/jquery/jquery.js"></script>
<script src="/lib/jquery-ui/jquery-ui.js"></script>
<script src="/lib/bootstrap/js/bootstrap.js"></script>
<script src="/lib/jquery-toggles/toggles.js"></script>

<script src="/lib/morrisjs/morris.js"></script>
<script src="/lib/raphael/raphael.js"></script>

<script src="/lib/flot/jquery.flot.js"></script>
<script src="/lib/flot/jquery.flot.resize.js"></script>
<script src="/lib/flot-spline/jquery.flot.spline.js"></script>

<script src="/lib/jquery-knob/jquery.knob.js"></script>

<script src="/js/quirk.js"></script>

<script src="/js/app.js"></script>

<script src="/lib/datatables/jquery.dataTables.js"></script>
<script src="/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script src="/lib/select2/select2.js"></script>