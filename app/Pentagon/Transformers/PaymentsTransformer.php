<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 5:36 AM
 */

namespace Transformers;


class PaymentsTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($payment)
    {
        return [
            'id'    => $payment['id']
        ];
    }
}