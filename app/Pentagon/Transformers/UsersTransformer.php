<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 5:38 AM
 */

namespace Transformers;


class UsersTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($user)
    {
        return [
            'id'    => $user['id']
        ];
    }
}