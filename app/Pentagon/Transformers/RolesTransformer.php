<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 5:37 AM
 */

namespace Transformers;


class RolesTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($role)
    {
        return [
            'id'    => $role['id']
        ];
    }
}