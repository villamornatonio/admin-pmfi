<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/13/15
 * Time: 1:33 PM
 */

namespace Transformers;


use Carbon\Carbon;

class TraineesTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($trainee)
    {
        return [
            'id'    => $trainee['id'],
            'name' => $trainee['firstname'] . ' ' . $trainee['lastname'],
            'position' => $trainee['position'],
            'firstname' => $trainee['firstname'],
            'middlename' => $trainee['middlename'],
            'lastname' => $trainee['lastname'],
            'passport_no' => $trainee['passport_no'],
            'seamans_book_no' => $trainee['seamans_no'],
            'mobile' => $trainee['mobile'],
            'dob' => Carbon::parse($trainee['dob'])->toFormattedDateString(),
            'email' => $trainee['email'],
            'emergency_person' => $trainee['emergency_person'],
            'emergency_contact' => $trainee['emergency_contact'],
        ];

    }
}