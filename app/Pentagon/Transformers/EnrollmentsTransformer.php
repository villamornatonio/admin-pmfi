<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 5:36 AM
 */

namespace Transformers;


class EnrollmentsTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($enrollment)
    {
        return [
            'id'    => $enrollment['id']
        ];
    }
}