<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/9/15
 * Time: 5:18 PM
 */

namespace Transformers;

abstract class Transformer
{


    /**
     * @param array $items
     * @return array
     */
    public function transformCollection(array $items)
    {

        return array_map([$this, 'transform'], $items);

    }

    /**
     * @param $item
     * @return mixed
     */
    public abstract function transform($item);

}