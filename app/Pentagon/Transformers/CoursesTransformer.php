<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 5:35 AM
 */

namespace Transformers;


class CoursesTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($course)
    {
        return [
            'id'    => $course['id']
        ];
    }
}