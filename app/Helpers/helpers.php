<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/13/15
 * Time: 12:58 PM
 */

if (!function_exists('pre')) {
    function pre($value, $exit = false)
    {

        echo '<pre>';
        print_r($value);
        echo '</pre>';

        if ($exit) {
            exit;
        }
    }
}

if (!function_exists('set_active')) {
    function set_active($path, $active = 'active')
    {

        return call_user_func_array('Request::is', (array)$path) ? $active : '';

    }
}