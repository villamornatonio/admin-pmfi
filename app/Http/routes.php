<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    pre(config('company.shortName'));
//    return view('welcome');
});


Route::resource('users','UsersController',['only' => ['create','store','edit','update','destroy','index']]);
Route::get('reports/daily', 'ReportsController@daily');
Route::get('reports/weekly', 'ReportsController@weekly');
Route::get('reports/monthly', 'ReportsController@monthly');
Route::resource('rooms','RoomsController',['only' => ['create','store','edit','update','destroy', 'index']]);
Route::resource('referrers','ReferrersController',['only' => ['create','store','edit','update','destroy', 'index']]);
Route::resource('trainees','TraineesController',['only' => ['create','store','edit','update','destroy','index']]);
Route::resource('courses','CoursesController',['only' => ['create','store','edit','update','destroy','index']]);
Route::resource('payments','PaymentsController',['only' => ['create','store','edit','update','destroy','index']]);
Route::get('payments/pending', 'PaymentsController@pending');
Route::resource('enrollments','UsersController',['only' => ['create','store','index']]);
Route::get('trainings/completed', 'TrainingsController@completed');
Route::get('trainings/unfinished', 'TrainingsController@unfinished');
Route::resource('trainings', 'TrainingsController',['only' => ['update']]);

Route::group(['prefix' => 'api/v1/'], function(){
    Route::get('rooms','RoomsController@getAll');
    Route::get('referrers','ReferrersController@getAll');
    Route::get('trainees','TraineesController@getAll');
    Route::get('courses','CoursesController@getAll');
    Route::get('enrollments','EnrollmentsController@getAll');
});