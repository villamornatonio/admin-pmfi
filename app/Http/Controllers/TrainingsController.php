<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


/**
 * Class TrainingsController
 * @package App\Http\Controllers
 */
class TrainingsController extends AdminController
{

    /**
     *
     */
    function __construct()
    {
    }


    /**
     * @return \Illuminate\View\View
     */
    public function completed()
    {

        $data = [];
        return view('trainings.completed',$data);

    }


    /**
     * @return \Illuminate\View\View
     */
    public function unfinished()
    {
        $data = [];
        return view('trainings.unfinished',$data);
    }

    public function update(Request $request, $id)
    {
        //
    }

}
