<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Class ReportsController
 * @package App\Http\Controllers
 */
class ReportsController extends AdminController
{

    /**
     *
     */
    function __construct()
    {
    }

    /**
     * @return \Illuminate\View\View
     */
    public function daily()
    {
        $data = [];

        return view('reports.daily', $data);

    }

    /**
     * @return \Illuminate\View\View
     */
    public function weekly()
    {
        $data = [];

        return view('reports.weekly', $data);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function monthly()
    {
        $data = [];

        return view('reports.monthly', $data);

    }

}
