<?php

namespace App\Http\Controllers;

use App\Models\Trainee;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Transformers;
use Transformers\TraineesTransformer;

class TraineesController extends AdminController
{
    protected $traineesTransformer;

    function __construct(TraineesTransformer $traineesTransformer)
    {
        $this->traineesTransformer = $traineesTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Input::has('limit')) {
            $this->setPaginationLimit(Input::get('limit'));
        }
        $trainees = Trainee::where('is_deleted', 0)->paginate($this->getPaginationLimit())->toArray();
        $data['trainees'] = $this->traineesTransformer->transformCollection($trainees['data']);

//        dd($data);
        return view('trainees.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('trainees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return view('trainees.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('trainees.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAll()
    {

    }
}
