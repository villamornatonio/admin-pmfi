<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Transformers\RoomsTransformer;
use yajra\Datatables\Facades\Datatables;

class RoomsController extends Controller
{
    protected $roomsTransformer;

    function __construct(RoomsTransformer $roomsTransformer)
    {
        $this->roomsTransformer = $roomsTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
//        $data['rooms'] = $this->roomsTransformer->transformCollection(Room::where('is_deleted', 0)->get()->toArray());
        $data = [];
        return view('rooms.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('rooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return view('rooms.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('rooms.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAll()
    {
        $rooms = Room::where('is_deleted',0)->get();

        return response()->json(['data' => $this->roomsTransformer->transformCollection($rooms->toArray())],200);


    }
}
