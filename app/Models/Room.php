<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{

    public function author(){

        return $this->belongsTo('App\User','created_by')->get();
    }
}
